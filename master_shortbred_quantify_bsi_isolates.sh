# !/usr/bin/env bash
indir=/data/avogadro/vbucci/data/Kelly_Duke_wgs/4Vanni/fastq
markers=/data/avogadro/vbucci/data/Kelly_Duke_wgs/shortbred_marker_data/ShortBRED_CARD_2017_markers.faa
res_dir=/data/avogadro/vbucci/data/Kelly_Duke_wgs/shortbred-output-bsi-isolates

if [ ! -d ${res_dir} ]; then
    mkdir -p ${res_dir}
fi

#for each in $(ls ${indir}/*/*pe_1* | sort -u);do
for each in $(ls ${indir}/*_R1* | sort -u);do
    echo $each
    sname=$(basename ${each})
    echo $sname
    echo "python ./shortbred_quantify.nv.bug.py --markers ${markers} --wgs ${each} --tmp ./shortbred_quantify_tmp --usearch /usr/bin/usearch --results ${res_dir}/${sname}.results.txt --threads 40"

    python ./shortbred_quantify.nv.bug.py --markers ${markers} --wgs ${each} --tmp /data/vbucci/shortbred_quantify_tmp --usearch /usr/bin/usearch --results ${res_dir}/${sname}.results.txt --threads 20
    #exit 1
done

#exit 1

echo "humann2_join_tables --input ${res_dir}/ --output shortbred_final_merged.txt"
humann2_join_tables --input ${res_dir}/ --output shortbred_final_merged.txt

mv ./shortbred_final_merged_bsi_isolates.txt ${res_dir}
